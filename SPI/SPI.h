/***************
Submit By Mapo
Time：2022.2.21
***************/

#define NSS_L			(GPIOB->BRR |= GPIO_Pin_12)				//GPIOB 的BRR寄存器，给对应为置一，对应位输出低电平
#define NSS_H			(GPIOB->BSRR |= GPIO_Pin_12)			//GPIOB 的BRR寄存器，给对应为置一，对应位输出低电平


#ifndef _SPI_H
#define _SPI_H

#include <STM32F10x.h>

void SPI2_Init(void);
u8 SPI2_ReadWriteByte(u8 data);


void SPI2_Write_Byte(u8 Reg,u8 data);
u8 SPI2_Read_Byte(u8 Reg);
u8 SPI2_Write_Buf(u8 Reg,u8 *Buf,u8 Len);
u8 SPI2_Read_Buf(u8 Reg,u8 *Buf,u8 Len);




#endif

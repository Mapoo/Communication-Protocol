/************************
2022/2/14
practise by mapo



IIC数据是从高位到低位的发送顺序
************************/



#include "i2c.h"
#include "delay.h"








//I2C初始化
void I2C_Init1(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  //定义GPIO配置结构体
 	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	 //使能PB端口时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;				 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD; 		 //开漏输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(GPIOB, &GPIO_InitStructure);					 
}


//开始动作
void I2C_Start(void)
{
	_I2C_Busy1();			//判断SDA状态是否是空闲状态
	SCL_L;			//SCL拉低再改变SDA拉高，防止变成停止信号
	Delay_us(1);
	SDA_H;
	Delay_us(1);
	SCL_H;
	Delay_us(1);
	SDA_L;			//在SCL为高时拉低SDA，完成开始动作
	Delay_us(1);
	
	SCL_L;			//将SCl拉低，为下一动作做准备，改变SDA状态
	Delay_us(1);
}

//停止动作
void I2C_Stop(void)
{
	//防止错误，因为不知道SDA的状态，所以这里拉低SDA
	SCL_L;
	Delay_us(1);
	SDA_L;
	Delay_us(1);
	//在SCL高时拉高SDA，完成停止动作
	SCL_H;
	Delay_us(5);	//SCL拉高>4.7us后再拉高SDA
	SDA_H;
	Delay_us(5);	//SDA拉高后>4.7us，完成停止动作
	
	SCL_L;			//拉低SCL为下一动作做准备
	Delay_us(1);
}


//主机产生应答或非应答信号
void I2C_SetACK(FunctionalState ackstate)
{
	SCL_L;
	Delay_us(1);
	//判断
	if(ackstate == ENABLE){
			SDA_L;				//应答
			Delay_us(1); 
	}else{
		SDA_H;					//非应答
		Delay_us(1);
	}
	SCL_H;
	Delay_us(1);
	 //释放SDA控制权
	SCL_L; 
	Delay_us(1);
	SDA_H;
	Delay_us(1);
}
	
//主机获取从机的应答信号
FunctionalState I2C_GetACK(void)
{
	FunctionalState ask;
	
	SCL_H;
	Delay_us(1);
	
	if(SDA_READ){
		ask = DISABLE; 
	}else{
		ask = ENABLE;
	}
	SCL_L; 
	Delay_us(1);
	return ask;
}



//写数据给从机，从机返回应答或非应答信号
FunctionalState I2C_WriteByte(u8 data)
{
	//数据发送
	u8 i;
	for(i=0;i<8;i++){
		SCL_L; 				//拉低SCL，为SDA下一次数据变化做准备
		Delay_us(1);
		if(data & 0X80){	//判断最高位，为1拉高SDA，否则拉低SDA
			SDA_H;
			Delay_us(1);
		}else{
			SDA_L;
			Delay_us(1);
		}
		//将date移位以便下一位发送，SCL拉高完成一位发送
		data <<=1;			//一位发送完成后，左移一位发送第二位
		Delay_us(1);
		SCL_H;
		Delay_us(1);
	}
	//主机释放SDA，让总线空闲
	SCL_L;
	Delay_us(1);
	
	SDA_H;		//释放总线，让从机接手发送应答信号
	Delay_us(1);
	//mpu6050发出应答
	return I2C_GetACK();
}

//读取从机发送的数据，主机返回应答或非应答信号
u8 I2C_ReadByte(FunctionalState askstate)
{
	u8 i;
	u8 data = 0x00;		//设定接受数据变量，先清空准备接受数据
	for(i=0;i<8;i++){
		SCL_H;
		Delay_us(1);
		data <<=1;		//这里对空的data左移一位是为后面接受数据后左移一位做准备
		if(SDA_READ){	//主机接手从机发送过来的第一位是数据的最高位，所以把数据左移
			data |= 0x01;	
		}
		//发送一位后，SDA需要等待SCl拉低进行数据切换
		SCL_L;
		Delay_us(1);
	}
	I2C_SetACK(askstate);
	return data;
}

//主机向从机寄存器写入一个字节数据 
/*
流程:开始信号->主机发送从机地址加写方向->从机应答
	->主机发送从机寄存器地址->从机应答->主机开始写入
	->写入完成后主机释放SDA总线->从机应答->停止信号
*/
u8 I2C_WriteByteToSlave(u8 addr,u8 reg,u8 data)			//reg为寄存器地址
{
	FunctionalState state;
	//开始
	I2C_Start();
	//发送地址后将返回值放入state
	state = I2C_WriteByte(addr<<1|0);
	if(state == ENABLE){							//判断是否应答
		state = I2C_WriteByte(reg);					//写入寄存器地址
		if(state == ENABLE){						//判断是否应答
			I2C_WriteByte(data);					//写入数据
			//写入完成后，产生停止信号，返回0
			I2C_Stop();
			return 0;
		}
	}
	//未应答，产生停止信号，返回1
	I2C_Stop();
	return 1;
}
	
//主机向从机寄存器写入多个数据，连续写入
u8 I2C_WritedataToSlave(u8 addr,u8 reg,u8 len,u8 *buf)				//len为写入数据的长度，
{
	FunctionalState state;
	u8 i;
	//开始
	I2C_Start();
	//发送地址后将返回值放入state
	state = I2C_WriteByte(addr<<1|0);								//主机发送从机地址加写方向
	if(state == ENABLE){											//判断是否应答
		state = I2C_WriteByte(reg);									//写入寄存器地址
		if(state == ENABLE){										//判断是否应答
			for(i=0;i<len;i++){										//发送长度为len的数据
				state = I2C_WriteByte(*(buf+i));					//这里读取主机的应答信号是为了保证在传输中每一次都成功，如果产生问题就立即停止
				if(state == DISABLE){								//判断是否应答
						I2C_Stop();									//未应答，停止信号，结束数据传输
						return 1;
				}
			}
			//传输结束，停止信号，返回0
			I2C_Stop();
			return 0;
		}
	}
	//未应答，停止，返1
	I2C_Stop();	
	return 1;
}

//主机读取从机寄存器中的数据
/*
流程:开始信号->主机发送从机地址加写方向->从机应答
	->主机发送从机寄存器地址->从机应答->发送开始信号
	->主机发送从机地址加读方向->从机应答->开始读取数据
	->主机发送不应答信号完成传输->停止信号
*/
u8 I2C_ReaddataToSlave(u8 addr,u8 reg,u8 *buf)
{
	FunctionalState state;
	//开始
	I2C_Start();
	//发送地址后将返回值放入state
	state = I2C_WriteByte(addr<<1|0);			//写地址
	if(state == ENABLE){						//判断是否应答
		state = I2C_WriteByte(reg);				//写入寄存器地址
		if(state == ENABLE){					//判断是否应答
			I2C_Start();						//重新开始
			state = I2C_WriteByte(addr<<1|1);	//发送地址和变为读方向
			if(state == ENABLE){
				*buf = I2C_ReadByte(DISABLE);	//读取完成后，主机向从机发送未应答信号结束传输
				//传输完成，返0
				I2C_Stop();
				return 0;
			}
		}
	}
	//未应答，停止，返1
	I2C_Stop();	
	return 1;
}

//主机在从机寄存器中读取多个数据，连续读取
u8 I2C_ReadSomedataToSlave(u8 addr,u8 reg,u8 len,u8 *buf)
{
	FunctionalState state;
	//开始
	I2C_Start();
	//发送地址后将返回值放入state
	state = I2C_WriteByte(addr<<1|0);
	if(state == ENABLE){											//判断是否应答
		state = I2C_WriteByte(reg);								//写入要读取的寄存器地址
		if(state == ENABLE){										//判断是否应答
			I2C_Start();
			state = I2C_WriteByte(addr<<1|1);			//发送起始地址和变为读方向
			if(state == ENABLE){
				while(len){
					if(len == 1){
						*buf = I2C_ReadByte(DISABLE);				//读取最后一个数据后，主机向从机发送未应答信号结束传输
					}else{
						*buf = I2C_ReadByte(ENABLE);				//读取一个数据后，主机向从机发送应答信号，接受下一个数据
					}
					len--;											//下一个数据
					buf++;											//下一个地址
				}
				
				//传输完成，返0
				I2C_Stop();
				return 0;
			}
		}
	}
	//未应答，停止，返1
	I2C_Stop();	
	return 1;
}

//判断总线SDA是否处于空闲状态（高电平），若不是就循环提供SCl时钟，直到SDA空闲
void _I2C_Busy1(void)
{
	while(!I2C_SDA_READ)		//读取总线状态，SDA为空闲(为1)则走掉，为0进入循环
	{
		SCL_L;
		Delay_us(4);
		SCL_H;
		Delay_us(1);
	}
}





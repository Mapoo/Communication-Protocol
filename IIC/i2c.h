#ifndef _i2c_h
#define _i2c_h
#include "stm32f10x.h"

#define SCL_L			(GPIOB->BRR |= GPIO_Pin_6)		//BRR复位
#define SCL_H			(GPIOB->BSRR |= GPIO_Pin_6)		//BSRR置位
#define SDA_L			(GPIOB->BRR |= GPIO_Pin_7)
#define SDA_H			(GPIOB->BSRR |= GPIO_Pin_7)


#define SDA_READ			(GPIOB->IDR & GPIO_Pin_7)





void I2C_Init1(void);
void I2C_Start(void);
void I2C_Stop(void);

void I2C_SetACK(FunctionalState ackstate);
FunctionalState I2C_GetACK(void);

FunctionalState I2C_WriteByte(u8 data);
u8 I2C_ReadByte(FunctionalState askstate);

u8 I2C_WriteByteToSlave(u8 addr,u8 reg,u8 data);
u8 I2C_WritedataToSlave(u8 addr,u8 reg,u8 len,u8 *buf);
u8 I2C_ReaddataToSlave(u8 addr,u8 reg,u8 *buf);
u8 I2C_ReadSomedataToSlave(u8 addr,u8 reg,u8 len,u8 *buf);
void _I2C_Busy1(void);
	









#endif
